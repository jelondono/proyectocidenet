import { Request, Response } from 'express';


import pool from '../../database';

class AreasController {

    /* Listado de areas */

    public async list(req: Request, res: Response): Promise<void> {
        const areas = await pool.query('SELECT * FROM cdn_area');
        res.json(areas);
    }


}

const areasController = new AreasController;
export default areasController;