import { json, Request, Response } from 'express';


import pool from '../../database';

class EmpleadosController {

    /* Listado de empleados */
    public async list(req: Request, res: Response): Promise<void> {
        try {
            const empleados = await pool.query('SELECT * FROM cdn_empleados');
            res.json({ result: empleados });
        } catch (error) {
            res.status(500).json({ status: error.status, message: "Ocurrio un error al intentar listar los registros", result: error });
        }


    }

    /* Consultar empleado por ID */

    public async getOne(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const empleados = await pool.query('SELECT * FROM cdn_empleados WHERE idEmpleado = ?', [id]);
        console.log(empleados.length);
        if (empleados.length > 0) {
            return res.json(empleados[0]);
        }
        res.status(404).json({ message: "No existe ningun empleado con el identificador:  " + id });
    }

    /* Crear empleado */

    public async create(req: Request, res: Response): Promise<void> {
        try {
            const result = await pool.query('INSERT INTO cdn_empleados set ?', [req.body]);
            console.log(result)
            res.status(200).json({ status: 200, message: 'Se ha creado un nuevo empleado' });
        } catch (error) {
            console.error(error)
            res.status(500).json({ status: error.status, message: "Ocurrio un error al intentar crear un nuevo registro", result: error });
        }

    }

    /* Actualizar empleado */

    public async update(req: Request, res: Response): Promise<void> {

        try {
            const { id } = req.params;
            const beanEmpleadoActualizar = req.body;
            await pool.query('UPDATE cdn_empleados set ? WHERE idEmpleado = ?', [req.body, id]);
            res.status(200).json({ status: 200, message: "Se actualizo correctamente" });
        } catch (error) {
            console.error(error)
            res.status(500).json({ status: error.status, message: "Ocurrio un error al intentar actualizar el registro", result: error });
        }

    }


    /* Eliminar empleado */
    public async delete(req: Request, res: Response): Promise<void> {
        try {
            const { id } = req.params;
            await pool.query('DELETE FROM cdn_empleados WHERE idEmpleado = ?', [id]);
            res.status(200).json({ status: 200, message: "El empleado se ha eliminado de la base de datos" });
        } catch (error) {
            console.error(error)
            res.status(500).json({ status: error.status, message: "Ocurrio un error al intentar eliminar el registro", result: error });
        }

    }

    /* Validar si existe correo */

    public async getCorreo(req: Request, res: Response): Promise<any> {

        try {
            const { correo } = req.params;
            const respuestaCorreo = await pool.query('SELECT * FROM cdn_empleados WHERE correo = ?', [correo]);
            console.log(respuestaCorreo.length);
            if (respuestaCorreo.length > 0) {
                return res.json({ status: 200, message: 'Este correo existe' });
            } else {
                return res.json({ status: 404, message: 'No existe este correo' });
            }
        } catch (error) {
            res.status(500).json({ message: "Ocurrio un error al validar el correo" });

        }

    }
}

const empleadosController = new EmpleadosController;
export default empleadosController;