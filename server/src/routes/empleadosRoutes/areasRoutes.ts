import express, { Router } from 'express';
import areasController from '../../controllers/areas-controllers/areaController';


class AreasRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {
        this.router.get('/', areasController.list);
    }

}

export default new AreasRoutes().router;

