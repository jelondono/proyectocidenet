import express, { Router } from 'express';

import empleadosController from '../../controllers/empleados-controllers/empleadosController';

class EmpleadosRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {
        this.router.get('/', empleadosController.list);
        this.router.get('/:id', empleadosController.getOne);
        this.router.post('/', empleadosController.create);
        this.router.put('/:id', empleadosController.update);
        this.router.delete('/:id', empleadosController.delete);
        /* Metodos de validación de correos */
        this.router.get('/correo/:correo', empleadosController.getCorreo);
    }

}

export default new EmpleadosRoutes().router;

