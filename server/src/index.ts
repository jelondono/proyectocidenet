import express, { Application } from 'express'
import morgan from 'morgan'
import cors from 'cors'

import indexRoutes from './routes/indexRoutes'
import employesRoutes from './routes/empleadosRoutes/empleadosRoutes'
import areasRoutes from './routes/empleadosRoutes/areasRoutes'


class Server {
    /* Creacion del server */

    public app: Application;

    constructor() {
        this.app = express();
        this.config()
        this.routes()
    }

    config(): void {

        /* Definicion del puerto */
        this.app.set('port', process.env.PORT || 3000)

        /* Ver peticiones en consola */
        this.app.use(morgan('dev'))

        /* Habilitación de cors para angular */
        this.app.use(cors())

        /* Conversión de json */
        this.app.use(express.json())
        this.app.use(express.urlencoded({ extended: false }))
    }

    routes(): void {
        this.app.use(indexRoutes)
        this.app.use('/api/empleados', employesRoutes)
        this.app.use('/api/areas', areasRoutes)
    }

    start(): void {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server on port 3000', this.app.get('port'))
        })
    }
}


const server = new Server();
/* Inicializacion del server */
server.start();