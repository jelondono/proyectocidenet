-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-11-2020 a las 19:23:24
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_empleados`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cdn_area`
--

CREATE TABLE `cdn_area` (
  `idArea` int(11) NOT NULL,
  `nomArea` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cdn_area`
--

INSERT INTO `cdn_area` (`idArea`, `nomArea`) VALUES
(1, 'Administración'),
(2, 'Financiera'),
(3, 'Compras'),
(4, 'Operación'),
(5, 'Talento humano'),
(6, 'Servicios varios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cdn_empleados`
--

CREATE TABLE `cdn_empleados` (
  `idEmpleado` int(11) NOT NULL,
  `primerNombre` varchar(20) NOT NULL,
  `otrosNombres` varchar(50) NOT NULL,
  `primerApellido` varchar(20) NOT NULL,
  `segundoApellido` varchar(20) NOT NULL,
  `idPais` varchar(2) NOT NULL,
  `tipoIdentificacion` varchar(2) NOT NULL,
  `numeroIdentificacion` int(20) NOT NULL,
  `fecIngreso` date NOT NULL,
  `idArea` int(11) NOT NULL,
  `fecRegistro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `correo` varchar(300) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cdn_area`
--
ALTER TABLE `cdn_area`
  ADD PRIMARY KEY (`idArea`);

--
-- Indices de la tabla `cdn_empleados`
--
ALTER TABLE `cdn_empleados`
  ADD PRIMARY KEY (`idEmpleado`),
  ADD KEY `idArea` (`idArea`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cdn_area`
--
ALTER TABLE `cdn_area`
  MODIFY `idArea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cdn_empleados`
--
ALTER TABLE `cdn_empleados`
  MODIFY `idEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cdn_empleados`
--
ALTER TABLE `cdn_empleados`
  ADD CONSTRAINT `cdn_empleados_ibfk_1` FOREIGN KEY (`idArea`) REFERENCES `cdn_area` (`idArea`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
