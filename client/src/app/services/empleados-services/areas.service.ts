import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResponseFull } from '../../models/RespuestaModel';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AreasService {

    API_URI = 'http://localhost:3000/api';

    constructor(private http: HttpClient) { }

    getAreas() {
        return this.http.get(`${this.API_URI}/areas`);
    }



}
