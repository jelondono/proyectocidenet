import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResponseFull } from '../../models/RespuestaModel';
import { Observable } from 'rxjs';
import { GeneralInterface } from '../../models/General.model';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  API_URI = this.generalInterface.urlApi;

  constructor(private http: HttpClient, private generalInterface: GeneralInterface) {

  }

  getEmpleados() {

    return this.http.get<ResponseFull>(`${this.API_URI}/empleados`);
  }

  getEmpleadosPorId(id): Observable<ResponseFull> {
    return this.http.get(`${this.API_URI}/empleados/${id}`);
  }

  deleteEmpleado(id): Observable<ResponseFull> {
    return this.http.delete(`${this.API_URI}/empleados/${id}`);
  }

  saveEmpleado(empleado): Observable<ResponseFull> {
    return this.http.post(`${this.API_URI}/empleados`, empleado);
  }

  updateEmpleado(beanEmpleado): Observable<ResponseFull> {
    return this.http.put(`${this.API_URI}/empleados/${beanEmpleado.idEmpleado}`, beanEmpleado);
  }

  getCorreoEmpleado(correo): Observable<ResponseFull> {
    return this.http.get(`${this.API_URI}/empleados/correo/${correo}`);
  }

}
