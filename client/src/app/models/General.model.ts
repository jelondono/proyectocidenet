import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable()
export class GeneralInterface {
    titleWindowPlatform?: string;
    namePlatform?: string;
    urlApi?: string;
    urlWebSocket?: string;

    constructor() {
        this.titleWindowPlatform = 'EMPLEADOS CIDENET';
        this.namePlatform = 'EMPLEADOS CIDENET';
        this.urlApi = 'http://localhost:3000/api';
        this.urlWebSocket = 'http://localhost:3000/api';

    }

}




