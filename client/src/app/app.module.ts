import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { RegistroEmpleadosComponent } from './components/registro-empleados/registro-empleados.component';

import localeCO from '@angular/common/locales/es-CO';


// Services
import { EmpleadosService } from './services/empleados-services/empleados.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { AreasService } from './services/empleados-services/areas.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { GeneralInterface } from './models/General.model';
import { FuncionesGlobalesService } from './services/funcionesGlobales.service';
import { registerLocaleData } from '@angular/common';



registerLocaleData(localeCO, 'es');

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    RegistroEmpleadosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule

  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-CO' },
    EmpleadosService,
    AreasService,
    GeneralInterface,
    FuncionesGlobalesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
