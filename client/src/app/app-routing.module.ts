import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroEmpleadosComponent } from './components/registro-empleados/registro-empleados.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'api/empleados',
    pathMatch: 'full'
  },
  {
    path: 'api/empleados',
    component: RegistroEmpleadosComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
