import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { RegistroEmpleadosComponent } from './registro-empleados.component';

describe('RegistroEmpleadosComponent', () => {
  let component: RegistroEmpleadosComponent;
  let fixture: ComponentFixture<RegistroEmpleadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistroEmpleadosComponent],
      imports: [ReactiveFormsModule]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroEmpleadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
