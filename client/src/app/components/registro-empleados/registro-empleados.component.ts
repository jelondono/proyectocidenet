import { Component, OnInit, HostBinding } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

import { EmpleadosService } from '../../services/empleados-services/empleados.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal, { SweetAlertType } from 'sweetalert2';
import { AreasService } from 'src/app/services/empleados-services/areas.service';
import { DatePipe } from '@angular/common';
import { isNullOrUndefined } from 'util';
import { FuncionesGlobalesService } from 'src/app/services/funcionesGlobales.service'

@Component({
  selector: 'app-registro-empleados',
  templateUrl: './registro-empleados.component.html',
  styleUrls: ['./registro-empleados.component.css'],
  providers: [DatePipe]

})
export class RegistroEmpleadosComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  listadoEmpleados: any = [];
  listadoEmpleadosTemp: any = [];
  listadoAreas: any = [];
  formEmpleados: FormGroup;
  submitted = false;
  modalAgregarEmpleado;
  modalEditarEmpleado;
  filtroEmpleados;
  p = 1;

  fechaActual: any;
  fechaActualSinHora: Date = new Date();
  mesMinimoAnterior: Date = new Date();


  constructor(private empleadosService: EmpleadosService, private areasService: AreasService, private modalService: NgbModal, private _formBuilder: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private funcionesGlobalesService: FuncionesGlobalesService
  ) { }


  ngOnInit() {
    this.mesMinimoAnterior.setMonth(this.mesMinimoAnterior.getMonth() - 1)



    /* Inicializacion del bean empleados */
    this.formEmpleados = this._formBuilder.group({

      idEmpleado: [],
      primerApellido: ['', Validators.required],
      segundoApellido: ['', Validators.required],
      primerNombre: ['', Validators.required],
      otrosNombres: ['', Validators.required],
      idPais: ['', Validators.required],
      tipoIdentificacion: ['', Validators.required],
      numeroIdentificacion: ['', Validators.required],
      correo: ['', Validators.required,],
      fecIngreso: ['', Validators.required],
      fecRegistro: [''],
      idArea: ['', Validators.required],
      estado: ['']

    });

    /* Llamado e inicializaciones iniciales de empleados y maestros */
    this.getEmpleados();
    this.getAreas();

  }



  /* INICIO METODOS CRUD */

  /* Consulta de empleados */
  getEmpleados() {
    this.empleadosService.getEmpleados()
      .subscribe(
        res => {

          let resTemp = res.result;

          for (let index = 0; index < resTemp.length; index++) {
            if (resTemp[index].estado === 'S') {
              resTemp[index].estado = 'ACTIVO';
            } else if (resTemp[index].estado === 'N') {
              resTemp[index].estado = 'INACTIVO';
            }
          }
          this.listadoEmpleados = resTemp;
          this.listadoEmpleadosTemp = resTemp;
        },
        err => console.error(err)
      );
  }


  /* Metodo para eliminar empleado por id */
  deleteEmpleado(id) {
    Swal.fire({
      title: 'Esta seguro que desea eliminar este empleado?',
      text: "Recuerde que una vez eliminado no se puede recuperar.!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {

      if (result.value === true) {
        debugger;
        this.empleadosService.deleteEmpleado(id)
          .subscribe(
            res => {
              let estado: SweetAlertType = (res.status === 200) ? 'success' : 'error';
              Swal.fire('Eliminar Empleado', res.message, estado);

              this.getEmpleados();
            },
            err => {
              Swal.fire('Error!', 'No se pudo eliminar correctamente, contactese con el administrador del sistema ', 'error')
              console.error(err)

            }
          )
      }

    })
  }
  /* Metodo para guardar nuevos empleados */
  async saveNuevoEmpleado() {
    this.formEmpleados.controls['estado'].setValue('S');

    const respuestaConversion = this.convertirUpperCase(true)
    await this.generarCorreoElectronico();


    if (respuestaConversion) {

      this.submitted = true;

      // parar guardado si hay errores en el form
      if (this.formEmpleados.invalid) {
        return;
      } else {

        this.empleadosService.saveEmpleado(this.formEmpleados.value)
          .subscribe(
            res => {

              let estado: SweetAlertType = (res.status === 200) ? 'success' : 'error';
              Swal.fire('Agregar Empleado', res.message, estado);
              if (res.status === 200) {
                this.modalAgregarEmpleado.close()
                this.formEmpleados.reset();


              }
              this.getEmpleados()

            },
            err => {
              Swal.fire('Error!', 'No se pudo actualizar correctamente, contactese con el administrador del sistema ', 'error')
              console.error(err)

            }
          )
      }
    }

  }

  /* Metodo de actualizacion de empleado */

  async updateEmpleado() {
    this.formEmpleados.controls['estado'].setValue('S');

    const respuestaConversion = this.convertirUpperCase(true)

    await this.generarCorreoElectronico();


    if (respuestaConversion) {

      this.submitted = true;

      // detener guardado si hay errores en el form
      if (this.formEmpleados.invalid) {
        return;
      } else {
        this.empleadosService.updateEmpleado(this.formEmpleados.value)
          .subscribe(
            res => {
              let estado: SweetAlertType = (res.status === 200) ? 'success' : 'error';
              Swal.fire('Actualizar Empleado', res.message, estado);
              if (res.status === 200) {
                this.modalEditarEmpleado.close()
                this.formEmpleados.reset();
              }
              this.getEmpleados()


            },
            err => {
              Swal.fire('Error!', 'No se pudo actualizar correctamente, contactese con el administrador del sistema ', 'error')
              console.error(err)

            }


          )
      }

    }


  }
  /* Consultar maestro de areas  */

  getAreas() {
    this.areasService.getAreas()
      .subscribe(
        res => {
          this.listadoAreas = res;
        },
        err => console.error(err)
      );
  }

  /* FIN METODOS CRUD */



  /* INICIO METODOS FUNCIONALES  */


  cerrarModalEditar() {
    this.formEmpleados.reset()
    this.modalEditarEmpleado.close()

  }
  cerrarModalGuardar() {
    this.submitted = false;
    this.formEmpleados.reset()
    this.modalAgregarEmpleado.close()
  }
  get f() { return this.formEmpleados.controls; }


  /* Instancia para crear modal */
  async openModalAgregarEmpleado(agregarEmpleado) {
    this.submitted = false;
    this.fechaActual = null;

    /* Inicializar la fecha para que quede en el registro */
    this.fechaActual = await this.funcionesGlobalesService.formatDate()
    this.formEmpleados.controls['fecRegistro'].setValue(this.fechaActual);
    this.modalAgregarEmpleado = this.modalService.open(agregarEmpleado, { windowClass: 'globalWrapperModal', backdrop: 'static', keyboard: false });

  }
  /* Instancia para editar modal */
  openModalEditarEmpleado(editarEmpleado, beanEmpleado) {
    /* Limpiado  de datos pasados */
    this.formEmpleados.reset();
    /* Llenado form de ediccion */
    this.formEmpleados.controls['idEmpleado'].setValue(beanEmpleado.idEmpleado);
    this.formEmpleados.controls['primerApellido'].setValue(beanEmpleado.primerApellido);
    this.formEmpleados.controls['segundoApellido'].setValue(beanEmpleado.segundoApellido);
    this.formEmpleados.controls['primerNombre'].setValue(beanEmpleado.primerNombre);
    this.formEmpleados.controls['otrosNombres'].setValue(beanEmpleado.otrosNombres);
    this.formEmpleados.controls['correo'].setValue(beanEmpleado.correo);
    this.formEmpleados.controls['idPais'].setValue(beanEmpleado.idPais);
    this.formEmpleados.controls['tipoIdentificacion'].setValue(beanEmpleado.tipoIdentificacion);
    this.formEmpleados.controls['numeroIdentificacion'].setValue(beanEmpleado.numeroIdentificacion);
    this.formEmpleados.controls['fecIngreso'].setValue(beanEmpleado.fecIngreso);
    this.formEmpleados.controls['fecRegistro'].setValue(beanEmpleado.fecRegistro);
    this.formEmpleados.controls['idArea'].setValue(parseInt(beanEmpleado.idArea));
    this.formEmpleados.controls['estado'].setValue(beanEmpleado.estado);


    this.modalEditarEmpleado = this.modalService.open(editarEmpleado, { windowClass: 'globalWrapperModal', backdrop: 'static', keyboard: false });

  }
  /* Metodo para asegurar que todo este en mayuscula */
  convertirUpperCase(closeLoading) {
    let respuesta = false;
    if (closeLoading) {
      this.formEmpleados.controls['primerApellido'].setValue(this.formEmpleados.get('primerApellido').value.toUpperCase());
      this.formEmpleados.controls['segundoApellido'].setValue(this.formEmpleados.get('segundoApellido').value.toUpperCase());
      this.formEmpleados.controls['primerNombre'].setValue(this.formEmpleados.get('primerNombre').value.toUpperCase());
      this.formEmpleados.controls['otrosNombres'].setValue(this.formEmpleados.get('otrosNombres').value.toUpperCase());
      this.formEmpleados.controls['tipoIdentificacion'].setValue(this.formEmpleados.get('tipoIdentificacion').value.toUpperCase());
      this.formEmpleados.controls['numeroIdentificacion'].setValue(this.formEmpleados.get('numeroIdentificacion').value.toString().toUpperCase());
      this.formEmpleados.controls['estado'].setValue(this.formEmpleados.get('estado').value.toUpperCase());

      respuesta = true;
    }
    return respuesta;
  }

  /* Filtro por varios items empleados */

  filtrarEmpleados(event) {
    this.listadoEmpleados = this.listadoEmpleadosTemp;
    let filtro = (event.target.value).toLowerCase();

    if (!isNullOrUndefined(filtro) && filtro != "") {
      this.listadoEmpleados = (this.listadoEmpleadosTemp).filter((itemEmpleado) => {
        let primerNombreArray: string = (itemEmpleado.primerNombre).toLowerCase();
        let segundoNombreArray: string = (itemEmpleado.otrosNombres).toLowerCase();
        let primerApellidoArray: string = (itemEmpleado.primerApellido).toLowerCase();
        let otrosNombresArray: string = (itemEmpleado.segundoApellido).toLowerCase();
        let tipoIdentificacionArray: string = (itemEmpleado.tipoIdentificacion).toLowerCase();
        let numeroIdentificacionArray: string = (itemEmpleado.numeroIdentificacion).toString().toLowerCase();
        let paisEmpleoArray: string = (itemEmpleado.idPais).toLowerCase();
        let correoElectronicoArray: string = (itemEmpleado.correo).toLowerCase();
        let estadoArray: string = (itemEmpleado.estado).toLowerCase();

        let resultadoFiltroPrimerNombreArray = (!isNullOrUndefined(primerNombreArray)) ? (primerNombreArray).indexOf(filtro) : -1;
        let resultadoFiltroSegundoNombreArray = (!isNullOrUndefined(segundoNombreArray)) ? (segundoNombreArray).indexOf(filtro) : -1;
        let resultadoFiltroPrimerApellidoArray = (!isNullOrUndefined(primerApellidoArray)) ? (primerApellidoArray).indexOf(filtro) : -1;
        let resultadoFiltroOtrosNombresArray = (!isNullOrUndefined(otrosNombresArray)) ? (otrosNombresArray).indexOf(filtro) : -1;
        let resultadoFiltroTipoIdentificacionArray = (!isNullOrUndefined(tipoIdentificacionArray)) ? (tipoIdentificacionArray).indexOf(filtro) : -1;
        let resultadoFiltroNumeroIdentificacionArray = (!isNullOrUndefined(numeroIdentificacionArray)) ? (numeroIdentificacionArray).indexOf(filtro) : -1;
        let resultadoFiltroPaisEmpleoArray = (!isNullOrUndefined(paisEmpleoArray)) ? (paisEmpleoArray).indexOf(filtro) : -1;
        let resultadoFiltroCorreoElectronicoArray = (!isNullOrUndefined(correoElectronicoArray)) ? (correoElectronicoArray).indexOf(filtro) : -1;
        let resultadoFiltroEstadoArray = (!isNullOrUndefined(estadoArray)) ? (estadoArray).indexOf(filtro) : -1;

        return resultadoFiltroPrimerNombreArray != -1 || resultadoFiltroSegundoNombreArray != -1 || resultadoFiltroPrimerApellidoArray != -1 || resultadoFiltroOtrosNombresArray != -1 || resultadoFiltroTipoIdentificacionArray != -1 || resultadoFiltroNumeroIdentificacionArray != -1 || resultadoFiltroPaisEmpleoArray != -1 || resultadoFiltroCorreoElectronicoArray != -1 || resultadoFiltroEstadoArray != -1;
      });
    }
  }


  /* Metodo que construye el correo electronico empresarial */
  async generarCorreoElectronico() {

    let primerNombre = this.formEmpleados.get('primerNombre').value.toLowerCase()
    let primerApellido = this.formEmpleados.get('primerApellido').value.toLowerCase().replace(/ /g, "")
    let dominio = this.formEmpleados.get('idPais').value.toLowerCase()

    let correoFull = primerNombre + '.' + primerApellido + '@' + 'cidenet.com.' + dominio;

    let respuestaTemp = null;
    let contador = 0;
    do {
      contador++

      this.empleadosService.getCorreoEmpleado(correoFull)
        .subscribe(
          res => {

            respuestaTemp = res.status;

            if (res.status === 404) {
              this.formEmpleados.controls['correo'].setValue(correoFull);
            }
            if (res.status === 200) {
              correoFull = primerNombre + '.' + primerApellido + '.' + contador + '@' + 'cidenet.com.' + dominio;
              this.formEmpleados.controls['correo'].setValue(correoFull);
            }
          },
        );
      await this.funcionesGlobalesService.delay(300)
    } while (respuestaTemp === 200)

  }

  /* Validación de fecha de registro */
  validarFechaSeleccion() {
    /* Conversion fecha selecccion */
    let s = this.formEmpleados.get('fecIngreso').value
    let info = s.split('-');
    let fechSeleccionString = info[2] + '/' + info[1] + '/' + info[0]



    /* Conversion Fecha Minima */

    let dd = String(this.mesMinimoAnterior.getDate()).padStart(2, '0');
    let mm = String(this.mesMinimoAnterior.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = this.mesMinimoAnterior.getFullYear();

    let fecMinString = dd + '/' + mm + '/' + yyyy;

    /* Conversion Fecha Actual */
    let dd1 = String(this.fechaActualSinHora.getDate()).padStart(2, '0');
    let mm1 = String(this.fechaActualSinHora.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy1 = this.fechaActualSinHora.getFullYear();

    let fechActualString = dd1 + '/' + mm1 + '/' + yyyy1;


    /* Comprobar fechas mayores o menores */
    if (this.funcionesGlobalesService.compararFechas(fecMinString, fechSeleccionString)) {
      Swal.fire('Error!', 'No se puede seleccionar una fecha menor', 'error')
      this.formEmpleados.controls['fecIngreso'].setValue('');

    } else if (this.funcionesGlobalesService.compararFechas(fechActualString, fechSeleccionString)) {

    } else {
      Swal.fire('Error!', 'No se puede seleccionar una fecha mayor a la actual', 'error')
      this.formEmpleados.controls['fecIngreso'].setValue('');

    }
    /* FIN METODOS FUNCIONALES  */
  }
}

